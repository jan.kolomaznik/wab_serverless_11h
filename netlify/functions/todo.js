const MONGO_URI = process.env['MONGO_URI']
if (!MONGO_URI) {
    throw new Error(`Missing enviroment variable MONGO_URI`)
}

import * as  mongoose from "mongoose";
const todoSchema = mongoose.Schema({
    created: { type: Date, default: Date.now },
    text: { type: String, require:true, trim: true },
    done: { type: Boolean, default: false }
})
const Todo = mongoose.model("Todo", todoSchema);

async function saveTodo(data) {
    let clinet = null;
    try {
        clinet = await mongoose.connect(MONGO_URI);
        const newTodo = new Todo(data);
        const savedTodo = await newTodo.save()
        return {
            statusCode: 201,
            body: JSON.stringify(savedTodo)
        }
    } finally {
        if (clinet) {
            await clinet.disconnect()
        }
    }
}

export const handler = async (event) => {
    switch (event.httpMethod) {
        case 'POST':
            return await saveTodo(JSON.parse(event.body))
        default:
            return {
                statusCode: 405,
                body: JSON.stringify("Method Not Allowed")
              };
    }
  };